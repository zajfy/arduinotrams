/*

TODO add array of letters to maybe in the future find the technology to convert strings to morse

*/
#include <ctype.h>

void setup() 
{
  // put your setup code here, to run once:
	pinMode(LED_BUILTIN, OUTPUT);
	pinMode(12, OUTPUT);
	pinMode(11,OUTPUT);
}

void loop() 
{
  // put your main code here, to run repeatedly:
	char *text = "test string";
	for(int i = 0; i < strlen(text);i++)
	{
		charTranslate(text[i]);
	}
  
}
//call funktions from switch alphabet
void charTranslate(char letter)
{
	letter = tolower(letter);
	switch(letter) 
	{
		case 'a':
			a();
			break;
		case 'b':
			b();
			break;
		case 'c':
			c();
			break;
		case 'd':
			d();
			break;
		case 'e':
			e();
			break;
		case 'f':
			f();
			break;
		case 'g':
			g();
			break;
		case 'h':
			h();
			break;
		case 'i':
			i();
			break;
		case 'j':
			j();
			break;
		case 'k':
			k();
			break;
		case 'l':
			l();
			break;
		case 'm':
			m();
			break;
		case 'n':
			n();
			break;
		case 'o':
			o();
			break;
		case 'p':
			p();
			break;
		case 'q':
			q();
			break;
		case 'r':
			r();
			break;
		case 's':
			s();
			break;
		case 't':
			t();
			break;
		case 'u':
			u();
			break;
		case 'v':
			v();
			break;
		case 'w':
			w();
			break;
		case 'x':
			x();
			break;
		case 'y':
			y();
			break;
		case 'z':
			z();
			break;	
		default:
			wordDelay();
			break;
	}
}
//add all letter functions to an array for easier access
//int (*letters[26])(void) {a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z}; //NVM was dumb idea in the first place

void wordDelay()
{
	digitalWrite(11,HIGH);
	delay(800);
	digitalWrite(11,LOW);
}

void longBlink()
{
	digitalWrite(12,HIGH);
	delay(600);
	digitalWrite(12,LOW);
	delay(200);
}

void shortBlink()
{
	digitalWrite(LED_BUILTIN,HIGH);
	delay(200);
	digitalWrite(LED_BUILTIN,LOW);
	delay(200);
}
void b()
{
	longBlink();
	shortBlink();
	shortBlink();
	shortBlink();
	delay(400);
}

void e()
{
	shortBlink();
	delay(400);
}

void f()
{
	shortBlink();
	shortBlink();
	longBlink();
	shortBlink();
	delay(400);
}

void h()
{
	for(int i = 0; i<4;i++)
	{
		shortBlink();
	}
	delay(400);
}

void j()
{
	shortBlink();
	for(int i = 0; i<3; i++)
	{
		longBlink();
	}
	delay(400);
}

void l()
{
	shortBlink();
	longBlink();
	shortBlink();
	shortBlink();
	delay(400);
}

void n()
{
	longBlink();
	shortBlink();
	delay(400);
}

void q()
{
	longBlink();
	longBlink();
	shortBlink();
	longBlink();
	delay(400);
}

void r()
{
	shortBlink();
	longBlink();
	shortBlink();
	delay(400);
}

void u()
{
	shortBlink();
	shortBlink();
	longBlink();
	delay(400);
}

void x()
{
	longBlink();
	shortBlink();
	shortBlink();
	longBlink();
	delay(400);
}

void y()
{
	longBlink();
	shortBlink();
	longBlink();
	longBlink();
	delay(400);
}

void a(){
    shortBlink();
    longBlink();
    delay(400);
}
void d (){
    longBlink();
    shortBlink();
    shortBlink();
    delay(400);
}
void g (){
    longBlink();
    longBlink();
    shortBlink();
    delay(400);
}
void i (){
    for (int i=0; i<2; i++){
    shortBlink();
    }
    delay(400);
}
void k(){
    longBlink();
    shortBlink();
    longBlink();
    delay(400);
}


void o(){
    for (int i=0; i<3; i++){
        longBlink();
    }
    delay(400);
}
void t(){
    longBlink();    
    delay(400);
}
void v (){
    for (int i = 0; i<3; i++){
    shortBlink();
    }
    longBlink();
    delay(400);
}

void c() 
{
  longBlink();
  shortBlink();
  longBlink();
  shortBlink();
  delay(400);
}

void m()
{
  longBlink();
  longBlink();
  delay(400);
}

void p()
{
  shortBlink();
  longBlink();
  longBlink();
  shortBlink();
  delay(400);
}

void s()
{
  shortBlink();
  shortBlink();
  shortBlink();
  delay(400);
}

void w()
{
  shortBlink();
  longBlink();
  longBlink();
  delay(400);
}

void z()
{
  longBlink();
  longBlink();
  shortBlink();
  shortBlink();
  delay(400);
}

